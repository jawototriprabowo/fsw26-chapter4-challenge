const playerChoice = document.querySelectorAll('.player');
const computerChoice = document.querySelectorAll('.computer');
const pilihan = [ 'batu', 'kertas', 'gunting' ];
const hasilGame = document.querySelector('#hasil');

playerChoice.forEach((choice) =>
	choice.addEventListener('click', () => {
		start();
		computerTurn();
		hasilGame.textContent = checkWin();
		document.getElementById('hasil').style.backgroundColor = 'green';
		document.getElementById('hasil').style.color = 'white';
		document.getElementById('hasil').style.fontSize = '20px';
		document.getElementById('hasil').style.borderRadius = '15px';
		document.getElementById('hasil').style.transform = 'rotate(-28.87deg)';
        
	})
);

function start() {
	let player1 = document.getElementsByClassName('player');

	for (let i = 0; i < player1.length; i++) {
		player1[i].addEventListener('click', () => {
			user = [ i ];
		});
	}
}

function computerTurn() {
	const randomCom = Math.floor(Math.random() * 3) + 1;

	switch (randomCom) {
		case 1:
			computer = 'batu';
			break;
		case 2:
			computer = 'kertas';
			break;
		case 3:
			computer = 'gunting';
			break;
	}

    
      
}


function checkWin() {
	let player = pilihan[user];

	if (player == computer) {
		return 'draw';
	} else if (player == 'batu') {
		return computer == 'kertas' ? 'computer win' : 'player win';
	} else if (player == 'kertas') {
		return computer == 'gunting' ? 'computer win' : 'player win';
	} else if (player == 'gunting') {
		return computer == 'batu' ? 'computer win' : 'player win';
	}
}
